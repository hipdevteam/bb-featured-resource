<?php
/**
 * Plugin Name: Beaver Builder Featured Resource Module
 * Plugin URI: http://www.mywebsite.com
 * Description: Custom modules for the Beaver Builder Plugin.
 * Version: 1.0
 * Author: Swakhar Dey
 */
define( 'FR_MODULES_DIR', plugin_dir_path( __FILE__ ) );
define( 'FR_MODULES_URL', plugins_url( '/', __FILE__ ) );

function bb_featured_resource_module() {
    if ( class_exists( 'FLBuilder' ) ) {
        // Include your custom modules here.
        require_once 'bb-featured-module/bb-featured-module.php';
    }
}
add_action( 'init', 'bb_featured_resource_module' );

?>