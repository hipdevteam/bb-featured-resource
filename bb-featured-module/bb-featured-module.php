<?php

class FLFeaturedResourceModule extends FLBuilderModule
{

    public function __construct()
    {
        parent::__construct(array(
            'name' => __('Featured Resource Module', 'fl-builder'),
            'description' => __('Featured resource module for bevar builder', 'fl-builder'),
            'category' => __('Advanced Modules', 'fl-builder'),
            'dir'             => FR_MODULES_DIR . 'bb-featured-module/',
            'url'             => FR_MODULES_URL . 'bb-featured-module/',
            'partial_refresh' => true // Defaults to false and can be omitted.
        ));
    }
}


FLBuilder::register_module( 'FLFeaturedResourceModule', array(
    'featured-resource-tab-1'      => array(
        'title'         => __( 'General', 'fl-builder' ),
        'sections'      => array(
            'general_settings'  => array(
                'title'         => __( 'General Settings', 'fl-builder' ),
                'fields'        => array(
                    'headline'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Headline', 'fl-builder' ),
                    ),
                    'tag_line'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Tag Line', 'fl-builder' ),
                    ),
                    'body_text' => array(
                        'type'          => 'editor',
                        'media_buttons' => true,
                        'rows'          => 10,
                        'label'         => __( 'Body Text', 'fl-builder' ),
                    ),
                    'featured_resource_image' => array(
                        'type'          => 'photo',
                        'label'         => __('Featured Resource Image', 'fl-builder'),
                        'default'       => '',
                        'show_remove'	=> false
                    ),
                    'fr_image_sizes' => array(
                        'type'          => 'photo-sizes',
                        'label'         => __('Photo Sizes Field', 'fl-builder'),
                        'default'       => 'medium'
                    ),
                    'button_text'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Button Text', 'fl-builder' ),
                    ),
                    'button_link' => array(
                        'type'          => 'link',
                        'label'         => __('Button Link', 'fl-builder')
                    )
                )
            )
        )
    ),

    'featured-resource-tab-2'      => array(
        'title'         => __( 'Style', 'fl-builder' ),
        'sections'      => array(
            'general'  => array(
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'layout'       => array(
                        'type'          => 'select',
                        'label'         => __('Layout', 'fl-builder'),
                        'default'       => 'compact',
                        'options'       => array(
                            'main_content'             => __('Main Content', 'fl-builder'),
                            'aside'             => __('Aside', 'fl-builder')
                        ),
                        'help' => __('Main Content is for full row, aside is for sidebar.', 'fl-builder')
                    )
                )
            ),

            'content_style'  => array(
                'title'         => __( 'Content Style', 'fl-builder' ),
                'fields'        => array(
                    'headline_color' => array(
                        'type'          => 'color',
                        'label'         => __( 'Headline Color', 'fl-builder' ),
                        'default'       => '26336f',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'headline_size' => array(
                        'type'          => 'text',
                        'label'         => __( 'Headline Font Size', 'fl-builder' ),
                        'default'       => '30',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'headline_align' => array(
                        'type'          => 'select',
                        'label'         => __( 'Headline Align', 'fl-builder' ),
                        'default'       => 'center',
                        'options'       => array(
                            'left'      => __( 'left', 'fl-builder' ),
                            'center'      => __( 'center', 'fl-builder' ),
                            'right'      => __( 'right', 'fl-builder' )
                        )
                    ),
                    'tag_line_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Tag Line Color', 'fl-builder' ),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'tag_line_size' => array(
                        'type'          => 'text',
                        'label'         => __( 'Tag Line Size', 'fl-builder' ),
                        'default'       => '24',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'tag_line_align' => array(
                        'type'          => 'select',
                        'label'         => __( 'Tag Line Align', 'fl-builder' ),
                        'default'       => 'center',
                        'options'       => array(
                            'left'      => __( 'left', 'fl-builder' ),
                            'center'      => __( 'center', 'fl-builder' ),
                            'right'      => __( 'right', 'fl-builder' )
                        )
                    ),
                    'body_text_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Body Text Color', 'fl-builder' ),
                        'default'       => '7b7d85',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'body_text_size' => array(
                        'type'          => 'text',
                        'label'         => __( 'Body Text Font Size', 'fl-builder' ),
                        'default'       => '16',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'body_text_align' => array(
                        'type'          => 'select',
                        'label'         => __( 'Body Text Align', 'fl-builder' ),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __( 'left', 'fl-builder' ),
                            'center'      => __( 'center', 'fl-builder' ),
                            'right'      => __( 'right', 'fl-builder' ),
                            'justify'      => __( 'justify', 'fl-builder' ),
                        )
                    )

                )
            ),
            'background_style'  => array(
                'title'         => __( 'Background Style', 'fl-builder' ),
                'fields'        => array(
                    'background_color' => array(
                        'type'          => 'color',
                        'label'         => __( 'Background Color', 'fl-builder' ),
                        'default'       => 'f0f5f7',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'border_width' => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Width', 'fl-builder' ),
                        'default'       => '1',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'border_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Border Color', 'fl-builder' ),
                        'default'       => '7b7d85',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    )
                )
            ),
            'button_style'  => array(
                'title'         => __( 'Button Style', 'fl-builder' ),
                'fields'        => array(
                    'button_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Button Color', 'fl-builder' ),
                        'default'       => 'fff',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'button_background_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Button Background Color', 'fl-builder' ),
                        'default'       => '00659c',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'button_hover_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Button Hover Color', 'fl-builder' ),
                        'default'       => 'fff',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'button_hover_background_color'       => array(
                        'type'          => 'color',
                        'label'         => __( 'Button Hover Background Color', 'fl-builder' ),
                        'default'       => '67c9e0',
                        'show_reset'    => true,
                        'show_alpha'    => false
                    ),
                    'button_align' => array(
                        'type'          => 'select',
                        'label'         => __( 'Button Align', 'fl-builder' ),
                        'default'       => 'center',
                        'options'       => array(
                            'left'      => __( 'left', 'fl-builder' ),
                            'center'      => __( 'center', 'fl-builder' ),
                            'right'      => __( 'right', 'fl-builder' ),
                        )
                    )
                )
            )
        )
    )
) );

?>