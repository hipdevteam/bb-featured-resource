<?php
$featured_class = 'fl-featured-resource ' . $settings->layout;
?>

<div class="<?php echo $featured_class; ?>">
    <h2 class="fl-featured-resource-headline"><?php echo $settings->headline; ?></h2>

    <div class="image-block">
        <div class="fl-featured-resource-image">
            <p>
                <?php echo wp_get_attachment_image($settings->featured_resource_image, $settings->fr_image_sizes); ?>
            </p>
        </div>
    </div>

    <div class="text-block">
        <h4 class="fl-featured-resource-tagline"><?php echo $settings->tag_line; ?></h4>

        <div class="fl-featured-resource-body">
            <p><?php echo $settings->body_text;?></p>
        </div>

        <div class="fl-featured-resource-button">
            <a href="<?php echo $settings->button_link;?>"><?php echo $settings->button_text;?></a>
        </div>
    </div>
</div>
