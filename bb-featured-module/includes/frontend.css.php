.fl-node-<?php echo $id; ?> .<?php echo $settings->layout; ?> {
    background-color: #<?php echo $settings->background_color; ?>;
    border: <?php echo $settings->border_width; ?>px solid #<?php echo $settings->border_color; ?>;
}

.fl-node-<?php echo $id; ?> h2.fl-featured-resource-headline {
    font-size: <?php echo $settings->headline_size; ?>px;
    color: #<?php echo $settings->headline_color; ?>;
    text-align: <?php echo $settings->headline_align; ?>;
    padding-bottom: 20px;
}

.fl-node-<?php echo $id; ?> h4.fl-featured-resource-tagline {
    font-size: <?php echo $settings->tag_line_size; ?>px;
    color: #<?php echo $settings->tag_line_color; ?>;
    text-align: <?php echo $settings->tag_line_align; ?>;
}

.fl-node-<?php echo $id; ?> .fl-featured-resource-body p {
    font-size: <?php echo $settings->body_text_size; ?>px;
    color: #<?php echo $settings->body_text_color; ?>;
    text-align: <?php echo $settings->body_text_align; ?>;
}

.fl-node-<?php echo $id; ?> .fl-featured-resource-button{
    text-align: <?php echo $settings->button_align; ?>;
}

.fl-node-<?php echo $id; ?> .fl-featured-resource-button a {
    background-color: #<?php echo $settings->button_background_color; ?>;
    color: #<?php echo $settings->button_color; ?>;
    display: inline-block;
    margin: 0 auto;
    text-align: center;
    padding: 15px;
}

.fl-node-<?php echo $id; ?> .fl-featured-resource-button a:hover {
    background-color: #<?php echo $settings->button_hover_background_color; ?>;
    color: #<?php echo $settings->button_hover_color; ?>;
}

<?php if($settings->layout == 'main_content') : ?>

.fl-featured-resource{
    float: left;
    box-sizing: border-box;
    padding: 35px;
}

.image-block{
    width: 50%;
    float: left;
    padding-right: 20px;
}

.text-block{
    width: 50%;
    float: left;
    padding-left: 20px;
}

@media(max-width: 480px){
    .fl-featured-resource{
        padding: 15px;
    }

    .image-block{
        display: block;
        width: 100%;
        padding-right: 0;
    }

    .text-block{
        display: block;
        width: 100% !important;
        padding-left: 0;
        text-align: center;
    }
}

<?php if($settings->featured_resource_image == '') : ?>

.image-block{
    display: none;
}
.text-block{
    display: block;
    width: 100%;
}

<?php endif ?>

<?php else: ?>

.fl-featured-resource{
    float: none;
    padding-bottom: 35px;
}
.image-block{
    padding-top: 20px;
    padding-bottom: 20px;
}
.fl-featured-resource-headline{
    position: relative;
}
.fl-featured-resource-headline:after{
    content: "";
    height: 1px;
    position: absolute;
    left: 25%;
    width: 50%;
    background: #fff;
    bottom: 0;
}
.fl-featured-resource-image img{
    display: block;
    margin: 0 auto;
}
.fl-featured-resource-tagline{
    display: block;
    padding-bottom: 20px !important;
}
.fl-featured-resource-body{
    display: none;
}

<?php endif ?>
